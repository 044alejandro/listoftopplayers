//
//  ViewController.swift
//  ListOfTopPlayers
//
//  Created by Виктор Сирик on 26.10.2021.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var topPlayersTableView: UITableView!
    var arrayOfPlayers: [TopPlayer] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topPlayersTableView.register(UINib(nibName: "TopPlayerCell", bundle: nil), forCellReuseIdentifier: "TopPlayerCell")    }
    
 
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showTopPlayers()

    }
    
    func findImage(imageURL: String) -> UIImage?{
        var image: UIImage?
        guard let url = URL(string: imageURL) else {
            print("incorrect URL")
            return nil
        }
        do {
            let data = try Data(contentsOf: url, options: [])
            image = UIImage(data: data)
        } catch{
            print(error.localizedDescription)
        }
        return image
    }
    
    func showTopPlayers () {
        var urlComponents = URLComponents(string: "https://api-football-beta.p.rapidapi.com/players/topscorers")!
        
        let leagueParametr = URLQueryItem(name: "league", value: "39")
        let seasonParametr = URLQueryItem(name: "season", value: "2019")
        
        urlComponents.queryItems = [leagueParametr, seasonParametr]
        
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = ["x-rapidapi-host": "api-football-beta.p.rapidapi.com",
                                       "x-rapidapi-key": "57fb1b76a9msh46392529f8fdcebp1b2bc0jsn05cc081835ca" ]
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            if let data = data{
                do{
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]{
                        if let responseKey = json["response"] as? [[String : Any]] {
                            for item in responseKey {
                                var newPlayer: TopPlayer = TopPlayer(firstName: "", height: "", lastName: "", nationality: "", teamName:"")
                                if let player = item["player"] as? [String : Any]{
                                    if let firstname = player["firstname"] as? String, let height = player["height"] as? String, let lastname = player["lastname"] as? String, let nationality = player["nationality"] as? String, let photo = player["photo"] as? String {
                                        newPlayer.firstName = firstname
                                        newPlayer.height = height
                                        newPlayer.lastName = lastname
                                        newPlayer.nationality = nationality
                                        newPlayer.imagePlayer = self.findImage(imageURL: photo)
                                    }
                                }
                                if let statistics = item["statistics"] as? [[String : Any]] {
                                    for obj in statistics{
                                    if let team = obj["team"] as? [String : Any] {
                                        if let name = team["name"] as? String, let logo = team["logo"] as? String {
                                            newPlayer.teamName = name
                                            newPlayer.logo = self.findImage(imageURL: logo)
                                        }
                                    }
                                    }
                                }
                                self.arrayOfPlayers.append(newPlayer)
                            }
                            DispatchQueue.main.async {
                                self.topPlayersTableView.reloadData()
                            }
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }.resume()
    }
}

