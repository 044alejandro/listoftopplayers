//
//  TopPlayerCell.swift
//  ListOfTopPlayers
//
//  Created by Виктор Сирик on 26.10.2021.
//

import UIKit

class TopPlayerCell: UITableViewCell {

    @IBOutlet weak var photoPlayerImage: UIImageView!
    @IBOutlet weak var logoOfPlayersClubImage: UIImageView!
    @IBOutlet weak var namePlayerLabel: UILabel!
    @IBOutlet weak var heightPlayerLavel: UILabel!
    @IBOutlet weak var lastNamePlayerLabel: UILabel!
    @IBOutlet weak var nationalityPlayerLabel: UILabel!
    @IBOutlet weak var teamNamePlayerLabel: UILabel!
    
    func update(player: TopPlayer) {
        namePlayerLabel.text = player.firstName
        heightPlayerLavel.text = player.height
        lastNamePlayerLabel.text = player.lastName
        nationalityPlayerLabel.text = player.nationality
        teamNamePlayerLabel.text = player.teamName
        logoOfPlayersClubImage.image = player.logo
        photoPlayerImage.image = player.imagePlayer
        
    }
    
}
