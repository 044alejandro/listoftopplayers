//
//  ViewController + Extensions.swift
//  ListOfTopPlayers
//
//  Created by Виктор Сирик on 26.10.2021.
//

import Foundation
import UIKit
extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfPlayers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TopPlayerCell", for: indexPath) as! TopPlayerCell
        cell.update(player: arrayOfPlayers[indexPath.row])
        return cell
    }
}
