//
//  TopPlayer.swift
//  ListOfTopPlayers
//
//  Created by Виктор Сирик on 26.10.2021.
//

import Foundation
import UIKit

struct TopPlayer {
    var firstName: String
    var height: String
    var lastName: String
    var nationality: String
    var teamName: String
    var logo: UIImage?
    var imagePlayer: UIImage?

}
